var Powersteer = require('powersteer');
var R = require('ramda');
var rpc;
var pushBullet;
function initialize(pushBulletRef)
{
	rpc = new Powersteer({
		//url: 'http://192.168.0.15:9091/transmission/rpc',
	    url: 'http://127.0.0.1:9091/transmission/rpc',
	    username: 'transmission',
	    password: 'cancer102'
	});
	pushBullet = pushBulletRef;
	

}

function addTorrent(src, callback)
{
	rpc.torrentAdd({filename: src, paused: false})
	.then((resp) => {
		callback(resp, null);
	})
	.catch((err) => {
		callback(null, err);
	});
}

function getTorrents(callback)
{
	rpc.torrentGet({fields: ['id', 'name', 'percentDone', 'rateDownload', 'rateUpload', 'eta', 'errorString', 'corruptEver', 'status', 'totalSize']})
	.then((resp) => {
		callback(resp, null);
	})
	.catch((err) => {
		console.log(err);
		callback(null, err);
	});
}

function torrentStart(id, callback)
{
	var ids = id ? {ids : id} : '';
	rpc.torrentStart([id])
	.then((resp) => {
		callback(resp, null);
	})
	.catch((err) => {
		console.log(err);
		callback(null, err);
	});
}

function torrentStop(id, callback)
{
	rpc.torrentStop(id)
	.then((resp) => {
		callback(resp, null);
	})
	.catch((err) => {
		console.log(err);
		callback(null, err);
	});
}

module.exports = {
	initialize: initialize,
	addTorrent: addTorrent,
	getTorrents: getTorrents,
	torrentStart: torrentStart,
	torrentStop: torrentStop
}