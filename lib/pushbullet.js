var PushBullet = require('pushbullet');
var PirateBay = require('../lib/piratebay');
var transmission = require('../lib/transmission');
var gitpull = require('git-pull');
var colorize = require('json-colorizer');
var prettyTime = require('pretty-seconds');
var filesize = require('filesize');


var pusher = new PushBullet('o.tfb6ayipt6HYaOxZs36qjI7uy599dZhG');
var devices = {
	torrentBox: 'ujx5q2TZwposjz5yqsZx4m'
};
var search = {
	results: {},
	selected: {},
	pagination: {
		start: 0,
		end: 5,
		isEnd: false
	}
};
var torrents = [];


var stream;
function initialize() {
	stream = pusher.stream(); 
	stream.connect();
	stream.on('connect', function() {
		pushMessage("Hello", "Connected to PushBullet! :)");
	});
	listenForEvents();
	
	transmission.initialize(this);
}
function listenForEvents()
{
	stream.on('tickle', function(message) {
		pusher.history({limit: 1}, function(err, res) {
			if (err){
				return console.log(err);
			}
			
			var push = res.pushes[0];
			if (!push)
				return;
			var isTargetDevice = (push.target_device_iden === devices.torrentBox); //Make sure event from torrentBox
			var isNotSelf = push["title"] == null; //Best way I can think to check if it's not sent by self. Impossible to set message with title from client
			if (isTargetDevice && isNotSelf)
			{
				var message = push.body;
				var commandEnd = message.indexOf(' ') != -1 ? message.indexOf(' ') : message.length;
				var command = message.substring(0, commandEnd).trim().toLowerCase();
				var query = message.substring(commandEnd).trim();
				switch (command)
				{
					case 'search':
						searchTPB(query);
						break;
					case 'next':
						nextPage();
						break;
					case 'more':
						moreInfo(query);
						break;
					case 'update':
						update();
						break;
					case 'download':
						download(query);
						break;
					case 'list':
						listTorrents();
						break;
					case 'start':
						startTorrent(query);
						break;
					case 'stop':
						stopTorrent(query);
						break;
				}
				
			}
		});
	});
}


function searchTPB(query)
{
	pushMessage('Fetching search query...', 'Please wait');
	PirateBay.search(query, function(err, results) {
		if (err)
		{
			pushMessage('Search Results', err);
			return;
		}
		search = {
			results: results,
			selected: {},
			pagination: {
				start: 0,
				end: results.length >= 5 ? 5 : results.length,
				isEnd: false
			}		
		};
		var body = parseSearchResults(results, search.pagination.start, search.pagination.end);
		pushMessage('Search Results', body);
	});
	
}

function nextPage()
{
	IncrementPagination(); 
	var body = search.pagination.isEnd ? "There are no more results to show." : parseSearchResults(search.results, search.pagination.start, search.pagination.end);
	pushMessage('Search Results', body);
}

function moreInfo(i)
{
	if (!i)
		return pushMessage('Error', 'Please specify the ID of the torrent you wish to view');
	if (!isNumeric(i))
	{
		return pushMessage('Error',"Not a Number");
	}
	if ((i < 0) && (i > search.results.legnth))
	{
		return pushMessage('Error', "Not in Range. Must be between 0 and " + search.results.legnth);
	}
	if (!search.results)
	{
		return pushMessage('Error', "Search results not yet loaded.");
	}
	pushMessage('Fetching more info...', 'Please wait');
	PirateBay.getTorrent(search.results[i], function(err, result) {
		if (err)
		{
			return pushMessage('Error', parseTorrentInformation(err));
		}
		search.selected = result;
		pushMessage('Torrent Information', parseTorrentInformation(result));
	})
}

function update()
{
	gitpull('/torrentbox', function (err, consoleOutput) {
		if (err) {
			pushMessage("Error", consoleOutput);
		} else {
			pushMessage("Success!", consoleOutput);
		}
	});
}

function download(i)
{
	if (!i) //If no query given, download previously selected
	{
		if (!search.selected)
		{
			return pushMessage('Error', "Torrent not selected. Please specify which torrent you would like to download.");
		}
		pushMessage('Adding Torrent....', search.selected.name);
		transmission.addTorrent(search.selected.magnetLink, function(res, err)
		{
			if (err)
			{
				pushMessage('Failure', 'Please try again.');
			}
			pushMessage('Added Torrent', search.selected.name);
		});
	}
	if (!isNumeric(i))
	{
		return pushMessage('Error',"Not a Number");
	}
	if ((i < 0) && (i > search.results.legnth))
	{
		return pushMessage('Error', "Not in Range. Must be between 0 and " + search.results.legnth);
	}
	pushMessage('Adding Torrent....', search.results[i].name);
		transmission.addTorrent(search.results[i].magnetLink, function(res, err)
		{
			if (err)
			{
				pushMessage('Failure', 'Please try again.');
			}
			pushMessage('Added Torrent', search.results[i].name);
		});
}

function listTorrents()
{
	pushMessage('Fetching Active Torrents...', 'Please wait');
	transmission.getTorrents(function(result, err)
	{
		if (err)
		{
			return pushMessage('Cannot retrieve Torrent List', 'Try again');
		}
		pushMessage('Active Torrents', parseTorrentList(result));

	});
}

function startTorrent(i)
{
	pushMessage('Starting...', 'Please Wait');
	transmission.torrentStart('', function(res, err) {
		if (err)
		{
			console.log(err);
			return pushMessage('Cannot start Torrent', 'Failed');
		}
		console.log(res);
		pushMessage('Success', 'Started');
	});
}

function stopTorrent(i)
{
	pushMessage('Stopping...', 'Please Wait');
	transmission.torrentStop('', function(res, err) {
		if (err)
		{
			return pushMessage('Cannot stop Torrent', 'Failed');
		}

		pushMessage('Success', 'Stopped');
	});
}

function parseSearchResults(results, start, end)
{
	if (results.length == 0)
	{
		return "No results found";
	}
	if ((end > results.length) || (start > results.length) || (start < 0) || (end <= 0) || (start > end))
	{
		return "Invalid Pagination Range";
	}
	var body = "";
	for (var i=start;i<end;i++)
	{
		if (results[i].name)
		{
			body += "\r\n" + i + ": " + results[i].name + "\r\n seeders: " + results[i].seeders + " leechers: " + results[i].leechers;
		}
	}
	return body;
}

function parseTorrentInformation(result)
{
	console.log(result);
	var body = "\r\nName: " + result.name;
	body += "\r\nSize: " + result.size;
	body += "\r\nSeeders: " + result.seeders;
	body += "\r\nLeechers: " + result.leechers;
	body += "\r\nUpload Date: " + result.uploadDate;
	body += "\r\nDescription: " + result.description;
	return body;
}

function parseTorrentList(result)
{
	var body = "";
	torrents = result.arguments.torrents;
	for (var i=0;i<torrents.length;i++)
	{
		var torrent = torrents[i];
		console.log(torrent);
		if (torrent.percentDone != 1)
		{
			var percentBarBlockCount = 20;
			var percentBarBlockCompleteChar = "#";
			var percentBarBlockIncompleteChar = '-';
			var statusCodes = ['Stopped',
				'Queued to check files',
				'Checking Files',
				'Queued to download',
				'Downloading',
				'Queued to Seed',
				'Seeding'
			];
			percentDone = Math.floor((torrent.percentDone * percentBarBlockCount));
			body += torrent.id + ": " + torrent.name;
			body += "\r\n[" + repeat(percentBarBlockCompleteChar, percentDone) + repeat(percentBarBlockIncompleteChar, percentBarBlockCount-percentDone) + "]";
			body += "\r\n" + (torrent.percentDone*100).toFixed(2) + "% of " + filesize(torrent.totalSize);
			if (torrent.corruptEver){
				body+= " (" + filesize(torrent.corruptEver) + ' corrupted)';
			}
			body += "\r\n" + statusCodes[torrent.status];
			if (torrent.eta > 0)
			{
				body += "\r\nDown: " + filesize(torrent.rateDownload) + "/s";
				body += " | Up: " + filesize(torrent.rateUpload) + "/s";
				body += "\r\n" + prettyTime(torrent.eta) + " remaining";
			}
			if (torrent.errorString){
				body += "\r\nERROR: " + torrent.errorString;
			}
			body += "\r\n---------------------------\r\n";
		}
	}
	return body;
}
function IncrementPagination()
{
	if (search.pagination.end + 5 < search.results.length)
	{
		search.pagination.start += 5;
		search.pagination.end += 5;
	} else {
		search.pagination.start = search.pagination.end;
		search.pagination.end = search.results.length;
		search.pagination.isEnd = true;
	}
}


function pushMessage(title, body)
{
	pusher.note(devices.torrentBox,title, body, function(err, res) {
		if (err) 
		{
			console.error(err);
		}
		console.log(res);
	});
}

function isNumeric(n) { return !isNaN(parseFloat(n)) && isFinite(n); }

function repeat(s, n) { var a=[],i=0;for(;i<n;)a[i++]=s;return a.join(''); }

function findActiveTorrentById(id) {
  for (var i=0; i<torrents.length; i++) {
    if (torrents[i].id == id) {
      return torrents[i];
    }
  }
}

module.exports = {
initialize: initialize,
pushMessage: pushMessage

};
