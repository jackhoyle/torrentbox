process.env.THEPIRATEBAY_DEFAULT_ENDPOINT = 'http://ahoy.one';

const PirateBay = require('thepiratebay');

function search(query, callback)
{
	PirateBay.search(query,
	{
		category: 'all'
	})
	.then(results => {
		console.log(results);
		callback(null, results);
	})
	.catch(err => {
		console.log(err);
		callback(err);
	})
}


function getTorrent(torrent, callback)
{
	PirateBay.getTorrent(torrent.id)
	.then(result => {
		callback(null, result);
		
	})
	.catch(error => {
		console.log(error, null);
	})
}

module.exports = {
	search: search,
	getTorrent: getTorrent
}